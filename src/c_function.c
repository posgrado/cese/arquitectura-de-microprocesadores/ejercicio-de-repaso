/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#include "c_function.h"

uint32_t c_max (int32_t * vectorIn, uint32_t longitud){
	uint32_t max = 0;
	for (uint32_t i = 0; i < longitud; i++) {
		if (vectorIn[max] < vectorIn[i]) {
			max = i;
		}
	}
	return max;
}

