/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/**
 * Directiva al ensablador que permite indicar que se encarga de buscar
 * la instruccion mas apropiada entre ARM y thumb2
 */
.syntax unified

/**
 * .text permite indicar una seccion de codigo.
 */
.text

/**
 * .global permite definir un simbolo exportable,
 * es decir que podemos verlo desde otros modulos (equivalente a extern).
 * Definimos la rutina como global para que sea visible desde otros modulos.
 */
.global asm_max

/**
 * Indicamos que la siguiente subrutina debe ser ensamblada en modo thumb,
 * entonces en las direcciones en el ultimo bit tendran el 1 para que se reconozcan como en modo thumb.
 * Siempre hay que ponerla antes de la primer instruccion.
 */
.thumb_func

/*=====[Definition macros of public constants]===============================*/

#define vectorIn	r0
#define longitud	r1
#define index_max	r4
#define value_max	r2
#define value		r3

/*=====[Implementations of public assembly functions]=================================*/

/**
 *	prototipo de la funcion en C
 *
 *	uint32_t asm_max(int32_t * vectorIn, uint32_t longitud);
*/
asm_max:
    push {r4, lr}  /* guardamos la direccion de retorno en la pila */
    subs longitud, 1 /* le resto 1 para empezar el loop*/
	mov index_max, longitud
    ldr value_max, [vectorIn, index_max, LSL 2]
	loop:
		ldr value, [vectorIn, longitud, LSL 2]
		cmp value_max, value
		bge less
		mov index_max, longitud
		ldr value_max, [vectorIn, longitud, LSL 2]
	less:
		subs longitud, 1
		bpl loop
	mov r0, index_max
	pop {r4, pc}   /* retorno */

	/* otras alternativas para el retorno */
	/* 1. mov pc,lr
	/  2. bx lr */
	/* pop {pc} */
