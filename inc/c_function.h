/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#ifndef __EJERCICIO_C_FUNCTION_H__
#define __EJERCICIO_C_FUNCTION_H__

#include "ejercicio.h"

/**
 * c_function do the same of ASM function for compare purpose
 * 
 * @param * vectorIn de 32 bits
 * @param longitud
 * @return max indice del maximo valor
 */
uint32_t c_max(int32_t * vectorIn, uint32_t longitud);

#endif /* __EJERCICIO_C_FUNCTION_H__ */
