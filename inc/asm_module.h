/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __EJERCICIO_ASM_MODULE_H__
#define __EJERCICIO_ASM_MODULE_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include "ejercicio.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[External data declaration]===========================================*/

/*=====[external functions declaration]======================================*/

/**
 * @brief C prototype function link with asm_module
 *
 * Automatically when you call and return from function, MCU utilizes these register:
 * 
 * |     C		|  CORE  |
 * | --------- 	| ------ |
 * |  vectorIn	|  r0 	 |
 * |  longitud	|  r1  	 |
 * |  return	|  r0    |
 * @param * vectorIn de 32 bits
 * @param longitud
 * @return max indice del maximo valor
 */
extern uint32_t asm_max(int32_t * vectorIn, uint32_t longitud);
/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __EJERCICIO_ASM_MODULE__ */
